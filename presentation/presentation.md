% Чистый код
% Параллель P
% ЛКЛ 2018


# Школы программировавия

 * Разные стили кодирования

Tabs vs spaces, где ставить скобочки...
 
\pause

* Олимпиадная школа (ВК, Telegram)
   
Однобуквенные переменные, фигак-фигак и в продакшн, отсутствие тестов...
 
\pause

* Не все стандарты одинаково хороши

# GNU Coding Standard


> First off, I'd suggest printing out a copy of the GNU coding standards,
> and NOT read it.  Burn them, it's a great symbolic gesture.

> Первым делом, Я советую распечатать копию стандарта кодирования GNU и НЕ читать их.
> Сожгите их, это хороший символический жест.

Linux kernel coding style, Linus Torvalds


*"Coding Horror"* -- Стив Макконнел, Совершенный код


# Что такое чистый код?

![Code quality](img/wtfm.jpg)


# Что такое чистый код?
*Есть разные мнения на этот счёт, но в целом...*

Свойства хорошего кода:

 * Простой и понятный (= читаемый)

 * Покрытый тестами (= корректный)

 * Изменяемость (= расширяемость)

 * Универсальность (= переиспользуемость)

# Зачем нужен чистый код?

 * Большие проекты
 * Большие команды
 * Длительное сопровождение

\pause

А может и ненужен вообще? (ВКонтакте, telegram)

# А стоит ли сразу писать хорошо?

А вы как считаете?
    

# А стоит ли сразу писать хорошо?

**Адвокат дъявола:**
Есть мнение, что, например, для стартапов важно скорее быстро что-нибудь нафигчать и выйти на рынок.

\pause

**Защитник бога:** можно не успеть переписать по-нормальному.

\pause

**Защитник бога:** плохой код приводит к техническому долгу, который, если не исправляется, приводит компанию к краху.


# Как же научится писать чистый код?

 * **ПРАКТИКА, ПРАКТИКА, ПРАКТИКА**

 * Пытаться делать из плохого кода хороший, рефакторить


# Modular Design Principles

 1. **Decomposability** — задача должна разбиваться на более простые подзадачи
 2. **Composability** — подзадачи должны быть самоценны и вне контекста задачи
 3. **Readability** — корректность кода модуля должна быть очевидна без изучения кода смежных модулей
 4. **Protection** — защита зависимых модулей от ошибок, происходящих внутри текущего модуля

*"Object oriented software construction", Meyer*

# DECOMPOSABILITY

# Задача: разбить CSV на поля

\small

```python
def split_to_fields(line: str) -> list[str]:
    pass

s='Field1  Field2  "Field 3 with spaces" "\"quote\""'

split_to_fields(s) == [
    'Field1',
    'Field2',
    'Field 3 with spaces',
    '"quote"'
]
```

# No decomposition

![No decomposition](img/no_decomposition.png)

# Маркеры плохой декомпозиции

> 1. Слишком длинные классы/методы (не умещаются на один экран)
> 2. Сложно придумать нормальное название/слишком общее названиее (`do_lots_of_things()`)
> 3. Слишком сложное название метода

# Decomposition

\footnotesize

```python

def read_fields(line: str) -> List[str]:
	
    def skip_spaces(line: str, from_pos: int) -> int
	def read_field(line: str, from_pos: int) -> int

        def read_simple_field(line: str, from_pos: int)
            -> (str, int)
		def read_quoted_field(line: str, from_pos: int)
            -> (str, int)
```

# COMPOSABILITY

* Python Standard Library

* Из примера про PasswordVerifier:

\footnotesize

```python
def at_least_n_ok(conditions, n):
    return sum(checks_results) >= n
```


# COMPOSABILITY

Из примера про PasswordVerifier:

\footnotesize

```python
def at_least_n_ok(conditions, n):
    def f(*args, **kwargs):
        check_results = (condition(*args, **kwargs) 
                           for condition in conditions)
        return sum(check_results) >= n
    return f
        
def all_conditions_ok(*conditions):
    def f(*args, **kwargs):
        check_results = (condition(*args, **kwargs) 
                           for condition in conditions)
        return all(check_results)
    return f
```

См. также: itertools

# Маркеры плохой компонуемости

1. Не самоценно


# READABILITY


# Маркеры плохой читаемости (и не только)

Статические данные:

 * статические поля классов
 * глобальные переменные

Не сломается ли в многопоточной среде?


# Маркер: скрытый поток данных

Bad:

```python
    input_data();
	solve();
	output_data();
```

Good:

```python
	data = input_data('input.txt')
	result = solve(data);
	output_data('output.txt', result)
```

\pause

Не прячьте поток данных от читателя!


# Маркер: ...


```python
def clear_full_lines(self):
    """Удалить все заполненные строки"""

```

![Тетрис](img/tetris.png)

# Маркер: ...

\footnotesize

```python
def clear_full_lines(self):
    """Удалить все заполненные строки"""

    for y in range(height):
        is_full = all(
                self.filled[x][y] for x in range(self.width) )
        if not is_full:
            continue

        for yy in range(y, self.height - 1):
            for x in range(self.width):
                self.filled[x][yy] = self.filled[x][yy+1]

        for x in range(self.width):
            self.filled[x, self.height-1] = False

```

<!--(монстрика сюда, или что-нибудь с %))-->


# Маркер: я так не объясняю

Пишите код так, как будете его объяснять коллеге!

# Маркер: я так не объясняю

\small

```python
def clear_full_lines(self):
    y = 0 # bottom
    while (y < self.height):
        if self.line_is_full(y):
            self.shift_down_all_lines_higher_than(y)
            self.add_empty_line_on_top()
        else:
            y += 1
```

# Immutable style


\footnotesize    
    
```python
def clear_full_lines(self: Field) -> Field:
    notfull_lines = self.get_all_notfull_lines()
    cleared_lines_count = self.height - len(notfull_lines)

    new_lines = create_new_lines_array(cleared_lines_count,
            notfull_lines)

    return Field(self.width, self.height,
        new_lines, self.score + cleared_lines_count)
}
```


# Маркер: ...

Исходный код тестирующей: https://goo.gl/LGUcux
<!-- https://github.com/Nazg-Gul/WebTester/blob/master/src/webtester/modules/informatics/tester.c#L669 -->

Функция выполняющая основной цикл тестирования:

* 500 строк
* до 5(?) уровней вложенности
* использует goto..
* ...


# Маркер: Ох, ...

\footnotesize

Есть другое название: "ох, хочу кофе"

![Oh god](img/ohmygod.jpg)


# Маркеры плохой читаемости

 1. Скрытый поток данных
 2. Я так не объясняю
 3. Ох.... 
 4. Чрезмерная навигация по коду


# Challenge!

https://gitlab.com/sicamp-py/clean-code

Задание на рефакторинг:

 * Работаем в парах
 * Небольшими изменениями улучшаем код, не ломая тесты
 * После каждого изменения - меняется человек за компом
 * Не забывайте запускать тесты!


# Чистый код

![Clean code](img/cleancode.jpg)

# Реальный код

![Real code](img/realcode.jpg)

# Правило бойскаута

Оставь место стоянки чище, чем оно было до твоего прихода.

![Бойскауты](img/boyscout.jpg)

# Доп. задание

https://gitlab.com/sicamp-py/cleancode-chess

